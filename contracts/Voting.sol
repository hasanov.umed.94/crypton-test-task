//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "hardhat/console.sol";

contract Voting {

    address owner;
    uint256 commission;

    struct Vote {
        string name;
        uint dateTimeStart;
        bool isActive;
        uint winnerReward;
        address payable winner;
        uint candidatesCount;
        address[] voters;
        mapping(address => bool) candidates;
        mapping(address => bool) voted;
        mapping(address => uint) votesCount;
    }

    mapping(string => Vote) votes;

    constructor() {
        owner = msg.sender;
    }

    modifier onlyOwner
    {
        require(msg.sender == owner, "Only owner can create a vote.");
        _;
    }

    function createVoting(address[] memory _candidates, string memory _voteName) external onlyOwner {
        require(bytes(votes[_voteName].name).length == 0, "Vote is already exists.");

        Vote storage vote = votes[_voteName];
        vote.name = _voteName;
        vote.isActive = true;
        vote.dateTimeStart = block.timestamp;
        vote.candidatesCount = _candidates.length;
        for (uint i = 0; i < _candidates.length; i++) {
            vote.candidates[_candidates[i]] = true;
        }
    }

    function vote(string memory _voteName, address payable _candidate) external payable {
        require(bytes(votes[_voteName].name).length > 0, "There are no vote with this name.");
        require(votes[_voteName].isActive, "Voting is finished");
        require(!votes[_voteName].voted[msg.sender], "You already voted");
        require(votes[_voteName].candidates[_candidate], "Unknown candidate");
        require(msg.value >= 10000000000000000, "Cost of voting is 0.01 ETH");

        votes[_voteName].votesCount[_candidate] += 1;
        votes[_voteName].voted[msg.sender] = true;
        votes[_voteName].voters.push(msg.sender);

        if (votes[_voteName].votesCount[_candidate] > votes[_voteName].votesCount[votes[_voteName].winner]) {
            votes[_voteName].winner = _candidate;
        }

        votes[_voteName].winnerReward += 9000000000000000;
        commission += 1000000000000000;
    }

    function finishVoting(string memory _voteName) external payable {
        require(votes[_voteName].isActive, "Voting is finished");
//        require(block.timestamp >= votes[_voteName].dateTimeStart + 3 days, "Voting can't end earlier than 3 days after the start");

        votes[_voteName].winner.transfer(votes[_voteName].winnerReward);
        votes[_voteName].isActive = false;
    }

    function sendCommissionToOwner() external onlyOwner {
        payable(owner).transfer(commission);
    }

    function isActive(string memory _voteName) external view returns (bool) {
        return votes[_voteName].isActive;
    }

    function getCount(string memory _voteName, address _candidate) external view returns (uint) {
        return votes[_voteName].votesCount[_candidate];
    }

    function getWinnerReward(string memory _voteName) external view returns (uint) {
        return votes[_voteName].winnerReward;
    }

    function getVoters(string memory _voteName) external view returns(address[] memory) {
        return votes[_voteName].voters;
    }

}
