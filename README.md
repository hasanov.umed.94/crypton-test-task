# Crypton Task Voting

---
- HardHat Tasks
```shell
npx hardhat create-voting --candidates --vote-name
npx hardhat vote --candidate --vote-name
npx hardhat finish-voting --vote-name
npx hardhat send-commission
npx hardhat in-active --vote-name
npx hardhat reward --vote-name
npx hardhat voters --vote-name
npx hardhat votes-count --candidate --vote-name
```
---

- Coverage 100%
