const {expect} = require("chai");
const {ethers} = require("hardhat");

describe("Voting", function () {

    let owner
    let candidate1
    let candidate2
    let candidate3
    let voter1
    let voter2
    let contract
    let voteName = "SomeVoteName";
    let voteName2 = "SomeVoteName2";

    beforeEach(async function() {
        [owner, candidate1, candidate2, candidate3, voter1, voter2] = await ethers.getSigners()
        const Voting = await ethers.getContractFactory("Voting")
        contract = await Voting.deploy()
        await contract.deployed()
        console.log(contract.address)
    })

    it("Should deploy Voting contract", async function () {
        expect(contract.address).to.be.properAddress
    })


    it("Should be active", async function () {
        await contract.createVoting([owner.address, candidate1.address], "Some Voting")
        let isActive = await contract.isActive("Some Voting")

        expect(isActive).to.equal(true)
    })


    it("Should be reverted when vote not active", async function() {
        await contract.createVoting([owner.address, candidate1.address], voteName)
        await contract.finishVoting(voteName);

        await expect(contract.vote(voteName, owner.address, { value: "10000000000000000" })).to.be.revertedWith('Voting is finished')
        await expect(contract.finishVoting(voteName)).to.be.revertedWith('Voting is finished')
    })


    it("Should be reverted when called by non-owner", async function() {
        await expect(contract.connect(candidate1).createVoting([owner.address, candidate1.address], voteName))
            .to.be.revertedWith("Only owner can create a vote.")
    })


    it("Should be reverted when voting with the same name was created", async function () {
        await contract.createVoting([owner.address, candidate1.address],voteName)

        await expect(contract.createVoting([owner.address, candidate1.address], voteName))
            .to.be.revertedWith("Vote is already exists.")
    })


    it("Should be reverted when voting name doesn't exist", async function () {
        await contract.createVoting([owner.address, candidate1.address], voteName)

        await expect(contract.vote(voteName2, candidate1.address, {value: "10000000000000000"}))
            .to.be.revertedWith("There are no vote with this name.")
    })

    it("Should be vote for not owner candidate", async function () {
        await contract.createVoting([owner.address, candidate1.address], voteName)
        await contract.vote(voteName, candidate1.address, {value: "10000000000000000"})

        expect(await contract.getWinnerReward(voteName)).to.equal(9000000000000000)
        expect(await contract.getCount(voteName, candidate1.address)).to.equal(1)
    })


    it("Should be reverted when already voted", async function () {
        await contract.createVoting([owner.address, candidate1.address], voteName)
        await contract.vote(voteName, candidate1.address, {value: "10000000000000000"})
        await expect(contract.vote(voteName, candidate1.address, {value: "10000000000000000"}))
            .to.be.revertedWith("You already voted")
    })

    it("Should be reverted when incorrect fee amount", async function () {
        await contract.createVoting([owner.address, candidate1.address], voteName)
        await expect(contract.vote(voteName, candidate1.address, {value: "9000000000000000"}))
            .to.be.revertedWith("Cost of voting is 0.01 ETH")
    })


    it("Should be reverted when unknown candidate", async function () {
        await contract.createVoting([owner.address, candidate1.address], voteName)
        await expect(contract.vote(voteName, candidate2.address, {value: "10000000000000000"}))
            .to.be.revertedWith("Unknown candidate")
    })

    it("Should be reverted when voting finished", async function () {
        await contract.createVoting([owner.address, candidate1.address], voteName)

        await contract.vote(voteName, candidate1.address, {value: "10000000000000000"})
        await contract.connect(voter1).vote(voteName, owner.address, {value: "10000000000000000"})
        await contract.connect(voter2).vote(voteName, candidate1.address, {value: "10000000000000000"})

        await contract.finishVoting(voteName);
        await expect(contract.vote(voteName, candidate1.address, {value: "10000000000000000"}))
            .to.be.revertedWith("Voting is finished")
    })

    it("Should be able to get list of voters", async function() {
        await contract.createVoting([owner.address, candidate1.address], voteName)

        await contract.connect(voter1).vote(voteName, candidate1.address, { value: "10000000000000000" })
        await contract.connect(voter2).vote(voteName, candidate1.address, { value: "10000000000000000" })

        let voters = await contract.getVoters(voteName)
        expect(voters[0]).to.eq(voter1.address)
        expect(voters[1]).to.eq(voter2.address)
    })

    it("Should sent commission successfully", async function () {
        await contract.createVoting([owner.address, candidate1.address], voteName)

        await contract.vote(voteName, candidate1.address, {value: "10000000000000000"})
        await contract.connect(voter1).vote(voteName, owner.address, {value: "10000000000000000"})
        await contract.connect(voter2).vote(voteName, candidate1.address, {value: "10000000000000000"})

        await contract.finishVoting(voteName);

        const ownerBalanceBefore = await ethers.provider.getBalance(owner.address);
        await contract.sendCommissionToOwner()

        const ownerBalanceAfter = await ethers.provider.getBalance(owner.address);
        expect(ownerBalanceBefore < ownerBalanceAfter)
    })
});
