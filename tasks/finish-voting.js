require('dotenv').config()

task("finish-voting", "Finish voting")
    .addParam("voteName", "Vote name")
    .setAction(async (taskArgs) => {
        const contract = await hre.ethers.getContractAt("Voting", process.env.CONTRACT_ADDRESS);
        await contract.finishVoting(taskArgs.voteName);

        console.log("Voting was finished successfully")
    });