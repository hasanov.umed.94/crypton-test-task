require('dotenv').config()

task("reward", "Winner reward")
    .addParam("voteName", "Vote name")
    .setAction(async (taskArgs) => {
        const contract = await hre.ethers.getContractAt("Voting", process.env.CONTRACT_ADDRESS);
        const result = await contract.getWinnerReward(taskArgs.voteName)

        console.log("Winner reward is: " + result)
    });