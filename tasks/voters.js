require('dotenv').config()

task("voters", "List of voters")
    .addParam("voteName", "Name of voting")
    .setAction(async (taskArgs) => {
        const contract = await hre.ethers.getContractAt("Voting", process.env.CONTRACT_ADDRESS);
        const result = await contract.getVoters(taskArgs.voteName)

        console.log("List of Voters: " + result)
    });