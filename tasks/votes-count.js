require('dotenv').config()

task("votes-count", "Votes count of candidate")
    .addParam("candidate", "Candidate's address")
    .addParam("voteName", "Vote name")
    .setAction(async (taskArgs) => {
        const contract = await hre.ethers.getContractAt("Voting", process.env.CONTRACT_ADDRESS);
        const result = await contract.getCount(taskArgs.voteName, taskArgs.candidate)

        console.log("For candidate " + taskArgs.candidate + " votes count: " + result)
    });