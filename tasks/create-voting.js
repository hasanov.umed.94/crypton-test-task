require('dotenv').config()

task("create-voting", "Creating a new voting")
    .addParam("candidates", "Address of candidates separated by , comma")
    .addParam("voteName", "Vote name")
    .setAction(async (taskArgs) => {
        const contract = await hre.ethers.getContractAt("Voting", process.env.CONTRACT_ADDRESS);
        await contract.createVoting(taskArgs.candidates.split(','), taskArgs.voteName)

        console.log("Voting successfully created: " + taskArgs.voteName)
    });