require('dotenv').config()

task("is-active", "Active voting")
    .addParam("voteName", "Vote name")
    .setAction(async (taskArgs) => {
        const contract = await hre.ethers.getContractAt("Voting", process.env.CONTRACT_ADDRESS);
        const result = await contract.isActive(taskArgs.voteName)

        console.log("Active voting: " + result)
    });