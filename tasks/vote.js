require('dotenv').config()

task("vote", "Vote to candidate")
    .addParam("voteName", "Name of voting")
    .addParam("candidate", "Candidate's address")
    .setAction(async (taskArgs) => {
        const contract = await hre.ethers.getContractAt("Voting", process.env.CONTRACT_ADDRESS);
        await contract.vote(taskArgs.voteName, taskArgs.candidate, {value: "10000000000000000"})

        console.log("Vote successfully added")
    });