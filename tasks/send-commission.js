require('dotenv').config()

task("send-commission", "Send commission to owner")
    .setAction(async () => {
        const contract = await hre.ethers.getContractAt("Voting", process.env.CONTRACT_ADDRESS);
        await contract.sendCommissionToOwner();

        console.log("Commission was successfully sent")
    });