require("@nomiclabs/hardhat-waffle");
require('solidity-coverage');
require('dotenv').config({path:__dirname+'/.env'})
require('./tasks/create-voting.js')
require('./tasks/vote.js')
require('./tasks/finish-voting.js')
require('./tasks/send-commission.js')
require('./tasks/is-active.js')
require('./tasks/reward.js')
require('./tasks/votes-count.js')
require('./tasks/voters.js')


task("accounts", "Prints the list of accounts", async (taskArgs, hre) => {
  const accounts = await hre.ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});

module.exports = {
  solidity: "0.8.4",
  defaultNetwork: "rinkeby",
  networks: {
    rinkeby: {
      url: process.env.URL,
      accounts: [process.env.PRIVATE_KEY],
      chainId: 4
    }
  }
};
