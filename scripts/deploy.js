const hre = require("hardhat");
const ethers = hre.ethers

async function main() {

    const Voting = await ethers.getContractFactory("Voting")
    let voting = await Voting.deploy()

    await voting.deployed()

    console.log("Voting contract deployed successfully! Address:", voting.address);
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
